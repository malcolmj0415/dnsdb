#!/usr/bin/python
# -*- coding: utf-8 -*-

import psycopg2
import sys
import re
import fileinput

def containsAll(domain_name):
  if re.search("<nodata>", domain_name, re.IGNORECASE) or re.search("NXDOMAIN",domain_name, re.IGNORECASE) or re.search("RecursionDesired", domain_name, re.IGNORECASE):
    return True
  splitdn = domain_name.split(".")
  for part in splitdn:
    if part == "dynetics":
      return True
  return False

try:
  RecordsId = 0
  DomainsId = 0
  AId = 0
  AAAAId = 0
  CNAMEId = 0
  SOAId = 0
  NSId = 0
  con = None
  splitline = ""
  con = psycopg2.connect(database='testdb', user='mj') 
  con.autocommit = True 
  cur = con.cursor()
  count = 0
  with open("dns.log-20160222") as f:
    query = """INSERT INTO Records(date, time, domainid, aid, aaaaid, cnameid, soaid, nsid) VALUES(%s, %s, %s, %s, %s, %s, %s, %s)"""
    insert_domain = """INSERT INTO Domains(domain_name) VALUES(%s)"""
    select_domain = """SELECT ID FROM Domains WHERE DOMAIN_NAME = '%s'"""
    for line in f:
      if count == 1000:
        break
      splitline = line.split("[**]")
      timestamp = splitline[0].replace(" ","")
      timestamp = timestamp.split("-")
      time = timestamp[1].replace(" ", "")
      date = timestamp[0].replace(" ","")
      RorQ = splitline[1].replace(" ","")
      RorQ = RorQ[:8]
      domain_name = splitline[2].replace(" ","")
      Answer = ""
      TTL = ""
      ip = ""
      if containsAll(domain_name) == True:
        continue
      if len(splitline) >= 6:
        Answer = splitline[3].replace(" ", "")
        TTL = splitline[4].replace(" " , "")
        ip = splitline[5].replace(" ", "")
        if re.search("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$",ip,re.IGNORECASE):
          cur.execute("SELECT ID FROM A WHERE IP = '%s'" % (ip))
          if cur.fetchone() == None:
            cur.execute("INSERT INTO A(ip) VALUES(%s)" % (ip))
            A_Id = cur.fetchone()
          else:
            A_Id = cur.fetchone()
          cur.execute(select_domain,(domain_name))
          if cur.fetchone() == None:
            cur.execute(insert_domain, (domain_name))
            Domains_Id = cur.fetchone()
          else:
            Domains_Id = cur.fetchone()
          cur.execute(query,(date,time,Domains_Id,A_Id,None,None,None,None))
          count = count +1
          print "Entered a A reply into Records..."
        if re.search("^(?:[A-F0-9]{1,4}:){7}[A-F0-9]{1,4}$",ip,re.IGNORECASE):
          cur.execute("SELECT ID FROM AAAA WHERE IP = '%s'" % (ip))
          if cur.fetchone() == None:
            cur.execute("INSERT INTO AAAA(ip) VALUES(%s)" % (ip))
            AAAA_Id = cur.fetchone()
          else:
            AAAA_Id = cur.fetchone()
          cur.execute(select_domain,(domain_name))
          if cur.fetchone() == None:
            cur.execute(insert_domain, (domain_name))
            Domains_Id = cur.fetchone()
          else:
            Domains_Id = cur.fetchone()
          cur.execute(query, (date,str(time),Domains_Id,None,AAAA_Id,None,None,None))
          count = count +1
          print "Entered a AAAA reply into Records..."  
        if Answer == "CNAME":
          cur.execute("SELECT ID FROM CNAME WHERE DOMAIN_NAME = '%s'" % (ip))
          if cur.fetchone() == None:
            cur.execute("INSERT INTO CNAME(domain_name) VALUES(%s)" % (ip))
            CNAME_Id = cur.fetchone()
          else:
            CNAME_Id = cur.fetchone()
          cur.execute(select_domain, (domain_name))
          if cur.fetchone() == None:
            Domains_Id = cur.fetchone()
            cur.execute(insert_domain, (domain_name))
          else: 
            continue
          cur.execute(query, (date,time,Domains_Id,None,None,CNAME_Id,None,None))
          count = count +1
          print "Entered a CNAME reply into Records..."
        if Answer == "SOA":
          cur.execute("SELECT ID FROM SOA WHERE DOMAIN_NAME = '%s'" % (ip))
          if cur.fetchone() == None:
            cur.execute("INSERT INTO SOA(domain_name) VALUES('%s')" % (ip))
            SOA_Id = cur.fetchone()
          else:
            SOA_Id = cur.fetchone()
          cur.execute(select_domain, (domain_name))
          if cur.fetchone() == None:
            cur.execute(insert_domain, (domain_name))
            Domains_Id = cur.fetchone()
          else:
            Domains_Id = cur.fetchone()
          cur.execute(query, (date,str(time),Domains_Id,None,None,None,SOA_Id,None))
          count = count + 1
          print "Entered a SOA reply into Records..."
        if Answer == "NS":
          cur.execute("SELECT ID FROM NS WHERE DOMAIN_NAME = '%s'" % (ip))
          if cur.fetchone() == None:
            cur.execute("INSERT INTO NS(domain_name) VALUES(%s)" % (ip))
            NS_Id = cur.fetchone()
          else:
            NS_Id = cur.fetchone()
          cur.execute(select_domain (domain_name))
          if cur.fetchone() == None:
            cur.execute(insert_domain, (domain_name))
            Domains_Id = cur.fetchone()
          else:
            Domains_Id = cur.fetchone()
          cur.execute(query, (date,time,Domains_Id,None,None,None,None,NS_Id))
          count = count +1
          print "Entered a NS reply into Records..."
      else:
        pass
        #print "Values set for Answer, TTL, and ip..."

except psycopg2.DatabaseError, e:
  print 'Error %s' % e    
  sys.exit(1)
    
finally:
  if con:
    con.close()
#!/usr/bin/python
# -*- coding: utf-8 -*-

import psycopg2
import sys
import re
import fileinput

def containsAll(domain_name):
  if re.search("<nodata>", domain_name, re.IGNORECASE) or re.search("NXDOMAIN",domain_name, re.IGNORECASE) or re.search("RecursionDesired", domain_name, re.IGNORECASE):
    return True
  splitdn = domain_name.split(".")
  for part in splitdn:
    if part == "dynetics":
      return True
  return False

con = None
splitline = ""
#myregex = r'(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-]{,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}'
#or re.search("^(?:[A-F0-9]{1,4}:){7}[A-F0-9]{1,4}$",ip,re.IGNORECASE)
try:
  Id = 0
  con = psycopg2.connect(database='testdb', user='mj') 
  con.autocommit = True 
  cur = con.cursor()
  cur.execute("SELECT * FROM Domains")
  rows = cur.fetchall()
  cur.execute("SELECT * FROM IPs")
  rows1 = cur.fetchall()
  cur.execute("SELECT * FROM Main")
  rows2 = cur.fetchall()
  with open("dns") as f:
    data = f.readlines()
    domlist = []
    iplist = []
    maindict = {}
    for line in data:
      splitline = line.split("[**]")
      timestamp = splitline[0].replace(" ","")
      RorQ = splitline[1].replace(" ","")
      RorQ = RorQ[:8]
      domain_name = splitline[2].replace(" ","")
      Answer = ""
      TTL = ""
      ip = ""
      if containsAll(domain_name) == True:
        continue
      if len(splitline) >= 6:
        Answer = splitline[3].replace(" ", "")
        TTL = splitline[4].replace(" " , "")
        ip = splitline[5].replace(" ", "")
      if Answer != "" and TTL != "" and ip != "":
        #Looks for IPv4 then IPv6 then reponse in the RorQ variable
        if re.search("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$",ip,re.IGNORECASE) and re.search("Response",RorQ,re.IGNORECASE) and re.search("[^SOCNME]A{1}", Answer, re.IGNORECASE):
          if ip in iplist or re.search("^(?:[A-F0-9]{1,4}:){7}[A-F0-9]{1,4}$",ip,re.IGNORECASE) :  #Going to skip IPv6 for right now
            pass
          else:
            iplist.append(ip)
            if ip in maindict:
              maindict[ip].append(domain_name)
            else:
              maindict[ip] = [domain_name]
      if domain_name in domlist:
        pass
      else:
        domlist.append(domain_name)
    for row in rows1: 
      if str(row[1]) in iplist:
        skip1 = str(row[1])
        iplist.remove(skip1)
    for row in rows:
      if row[1] in domlist:
        skip = row[1]
        domlist.remove(skip)
    for item in iplist:
      print "Adding ", item , "to IPs..."
      cur.execute("SELECT max(Id) FROM IPs")
      Ids = cur.fetchone()
      Id = Ids[0] + 1
      cur.execute("INSERT INTO IPs VALUES(%s, %s)",(Id,item))
    for item in domlist:
      print "Adding ", item , "to Domains..."
      cur.execute("SELECT max(Id) FROM Domains")
      Ids = cur.fetchone()
      Id = Ids[0] + 1
      cur.execute("INSERT INTO Domains VALUES(%s, %s)",(Id,item))


except psycopg2.DatabaseError, e:
  print 'Error %s' % e    
  sys.exit(1)
    
finally:
  if con:
    con.close()
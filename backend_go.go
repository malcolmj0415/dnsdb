import (
    "fmt"
    "strings"
)
var numGoWriters = 10


func processRow(r Row, ch chan<- string) {
    res := "What ever type of reply we are entering into records"
    splitline := strings.Split(r,"[**]")
    
    ch <- res
}

/*
Could make this where the actual writing of the queries happens. May not need this function. 
func writeRow(f File, ch <-chan string) {
    w := bufio.NewWriter(f)
    for s := range ch {
        _, err := w.WriteString(s + "\n")
    }
*/
func processFile(f File) {
    //outFile, err := os.Create("/path/to/file.out")
    /*if err != nil {
        // handle it
    }*/
    //defer outFile.Close()
    var wg sync.WaitGroup
    ch := make(chan string, 10)  // play with this number for performance
    defer close(ch) // once we're done processing rows, we close the channel
                    // so our worker threads exit
    fScanner := bufio.NewScanner(f)
    for fScanner.Scan() {
        wg.Add(1)
        go func() {
            processRow(fScanner.Text(), ch)
            wg.Done()
        }()
    }
    /* May not need this loop if WriteRow is removed
    for i := 0; i < numGoWriters; i++ {
        go writeRow(outFile, ch)
    }
    wg.Wait() 
    */ 
}

func main() {
    var wg sync.WaitGroup
    db, err := sql.Open("postgres", "user = mj, password = password, dbname = testdb, sslmode = disable")
    //instead of having an array of filenames here we can pass it a directory or some filename from the command line
    filenames := [...]string{"here", "are", "some", "log", "paths"}
    inFile, err := os.Open("dns.log-20160222")
    if err != nil {
        log.Fatal(err)
    }
    defer inFile.Close()
    wg.Add(1)
    go.processFile(infile)
/*    for fname := range filenames {
        inFile, err := os.Open(fname)
        if err != nil {
            // handle it
        }
        defer inFile.Close()
        wg.Add(1)
        go processFile(inFile)
    }*/
    wg.Wait()